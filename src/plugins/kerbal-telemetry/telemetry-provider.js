/*******************************************************************
 * MIT License
 *
 * Copyright (c) 2021-2022 Lukas Voreck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************/

let socket;
const listeners = {};

export default {
    init: function (ip, port) {
        socket = new WebSocket('ws://' + ip + ':' + port + '/datalink');

        socket.onmessage = function (event) {
            const data = JSON.parse(event.data);
            console.log(event.data);
            for (const k in data) {
                // Passes telemetry to the listeners

                if (k === "f.throttle") {
                    // Throttles telemetry needs to be multiplied by 100 so its usable as percentage
                    data[k] = data[k] * 100;

                } else if (k === "v.rcsValue"
                    || k === "v.sasValue"
                    || k === "v.lightValue"
                    || k === "v.brakeValue"
                    || k === "v.gearValue") {
                    // All of these need boolean telemetry
                    if (data[k] === true) {
                        data[k] = 1;
                    } else {
                        data[k] = 0;
                    }

                } else if (k.startsWith("r.")) {
                    // for some reason the very first value of the resource telemetry is often negative. this fixes
                    // that
                    if (data[k] < 0) {
                        data[k] = 0;
                    }
                }

                // Pass to listener
                listeners[k]({
                    timestamp: Date.now(),
                    value: data[k],
                    id: k
                });
            }
        };
    },

    provider: {
        supportsSubscribe: function (domainObject) {
            return domainObject.type === 'kerbal.telemetry';
        },

        subscribe: function (domainObject, callback) {
            let identifier;

            if (domainObject.identifier.key.startsWith("r.")) {
                const keyParts = domainObject.identifier.key.split(".");
                identifier = keyParts[0] + "." + keyParts[1] + "[" + keyParts[2] + "]";
            } else {
                identifier = domainObject.identifier.key;
            }

            listeners[identifier] = callback;
            socket.send(JSON.stringify({
                '+': [identifier]
            }));

            return function unsubscribe() {
                delete listeners[identifier];
                socket.send(JSON.stringify({
                    '-': [identifier]
                }));
            };
        }
    }
};
